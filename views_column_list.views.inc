<?php
/**
 * @file
 * Provide a column list display style for Views. This file is autoloaded by views.
 */

/**
  * Implementation of hook_views_plugin().
  */
function views_column_list_views_plugins() {
  return array(
    'style' => array(
      'views_column_list' => array(
        'title' => t('HTML column list'),
        'theme' => 'views_view_column_list',
        'help' => t('Display a list of rows themed as columns.'),
        'handler' => 'views_column_list_style_plugin',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
