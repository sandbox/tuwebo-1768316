<?php
/**
 * @file
 * Provide a column list display style for Views. This file is autoloaded by views.
 */

 /**
   * Implementation of views_plugin_style().
   */
class views_column_list_style_plugin extends views_plugin_style_list {
 /**
  * Set default options
  */
  function option_definition() {
    $options = parent::option_definition();
    $options['number_of_columns'] = array('default' => 1);
    $options['classes_per_column'] = array('default' => 0);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Only allows values greater than 0, avoiding division by 0 later on.
    $form['number_of_columns'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of columns'),
      '#description' => t('Set the number of columns to provide a custom-class for each row. First and last will have special classes.'),
      '#default_value' => $this->options['number_of_columns'] > 0 ? $this->options['number_of_columns'] : 1,
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['classes_per_column'] = array(
      '#type' => 'checkbox',
      '#title' => t('Insert classes per column'),
      '#description' => t('Specify whether to provide classes per column, like column-1, column-2, etc.'),
      '#default_value' => $this->options['classes_per_column'],
      '#required' => FALSE,
    );
  }
}
